from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    tape = input_
    state = machine["start state"]
    head = 0
    counter = 0
    hist = []

    while True:
        if state in machine["final states"]:
            break
        elif steps is not None and counter >= steps:
            break
        elif state not in machine["table"]:
            break

        if head < 0:
            tape = machine["blank"] + tape
            head = 0
        elif head >= len(tape):
            tape = tape + machine["blank"]

        sym = tape[head]
        if sym not in machine["table"][state]:
            break

        instr = machine["table"][state][sym]
        hist.append({"state": state, "reading": sym, "position": head, "memory": tape, "transition": instr})

        if instr == "L":
            head -= 1
        elif instr == "R":
            head += 1
        else:
            if "write" in instr:
                tape = tape[:head] + instr["write"] + tape[head + 1 :]
            if "L" in instr:
                head -= 1
                state = instr["L"]
            elif "R" in instr:
                head += 1
                state = instr["R"]
            else:
                break

        counter += 1

    return tape.strip(machine["blank"]), hist, state in machine["final states"]
